<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PromosTableSeeder::class);   // To run all datas
        $this->call(StudentsTableSeeder::class);
    }
}
